import './App.css'

import React, { Component } from 'react'

import { CountryDropdown, RegionDropdown } from 'react-country-region-selector'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { country: '', region: '' };
  }

  selectCountry(val) {
    this.setState({ country: val, region: '' })
  }

  selectRegion(val) {
    this.setState({ region: val });
  }

  render() {
    return (
      <div className='App' style={{ padding: '10px' }}>
        <div style={{ width: '50%', textAlign: 'left' }}>Select:</div>
        <div style={{ width: '50%', textAlign: 'left' }}>
          <CountryDropdown
            value={this.state.country}
            onChange={(val) => this.selectCountry(val)} />
        </div>
        <div style={{ width: '50%', textAlign: 'left' }}>
          <RegionDropdown
            country={this.state.country}
            value={this.state.region}
            onChange={(val) => this.selectRegion(val)} />
        </div>
        <div style={{ width: '50%', textAlign: 'left' }}>
          Current location:
            </div>
        <div style={{ width: '50%', textAlign: 'left' }}>
          {this.state.country && this.state.region ? this.state.region + ', ' + this.state.country : ''}
        </div>
      </div>
    )
  }
}

export default App
