# README #

### How do I get set up? ###

* Install latest Node.js
* Install PhantomJS and add the path to environment variable if you're running Windows 10
* IDE: Visual Studio Code

### How to run? ###
1. Go to /react-test-app
2. Run "npm install"
3. Run "npm run start"
4. Open "http://localhost:3000" in browser